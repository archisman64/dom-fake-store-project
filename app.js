const loader = document.querySelector('.loader');
const productRow = document.getElementById('product-row');
loader.style.display = 'block';

fetch('https://fakestoreapi.com/products')
    .then((result) => {
        return result.json();
    })
    .then((jsonData) => {
        loader.style.display = 'none';
        
        if(jsonData.length === 0) {
            productRow.innerHTML = `<h1>No products available!</h1>`;
            return;
        }
        jsonData.forEach((product) => {
            // console.log('product:', product)
            productRow.innerHTML += `
                <div class="product" id=${product.id}>
                    <div class="img-wrapper">
                        <img src=${product.image}></img>
                    </div>
                    <div class="info-wrapper">
                        <h3>${product.title}</h3>
                        <span class="price-style">Price: <strong>$ ${product.price}</strnog></span>
                        <p>${product.description.slice(0, 100)}...</p>
                        <span>Rating: ${product.rating.rate} - ${product.rating.count} votes</span>
                        <div id="purchase-btn-wrapper">
                            <button>Add to Cart</button>
                            <button>Buy Now</button>
                        </div>
                    </div>
                <div>
            `;
        })
        // console.log(jsonData)
    })
    .catch(err => {
        loader.style.display = 'none';
        console.log(err);
        productRow.innerHTML = `<h1>Error while loading products!</h1>`;
    })