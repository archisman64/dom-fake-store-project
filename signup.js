const signupForm = document.getElementById('signup-form');
const loader = document.querySelector('.loader');
loader.style.display = 'block';

window.addEventListener('DOMContentLoaded', (event) => {
    loader.style.display = 'none';
    signupForm.addEventListener('submit', (event) => {
        event.preventDefault();
    
        const tos = document.getElementById('tos');
        const errorMessage = document.getElementById('error-message');
    
        const firstNameErrorMessage = document.getElementById('first-name-err');
        const lastNameErrorMessage = document.getElementById('last-name-err');
        const emailErrorMessage = document.getElementById('email-err');
        const passwordErrorMessage = document.getElementById('password-err');
        const confirmPasswordErrorMessage = document.getElementById('confirm-password-err');
        const tosErrorMessage = document.getElementById('tos-err');
        
        const firstName = document.getElementById('first-name').value;
        const lastName = document.getElementById('last-name').value;
        const email = document.getElementById('email').value;
        const password = document.getElementById('password').value;
        const confirmPassword = document.getElementById('confirm-password').value;
    
        const isValid = validateSignupForm(firstName, firstNameErrorMessage, 
            lastName, lastNameErrorMessage,
            email, emailErrorMessage,
            password, passwordErrorMessage,
            confirmPassword, confirmPasswordErrorMessage,
            tos, tosErrorMessage);
    
        if(!isValid) {
            // errorMessage.textContent = 'Invalid value entered. Please check all the fields.';
            return;
        }
        errorMessage.textContent = '';
        console.log('Form successfully submitted!');
    });
})

function validateSignupForm(firstName, firstNameErrorMessage, 
    lastName, lastNameErrorMessage,
    email, emailErrorMessage,
    password, passwordErrorMessage,
    confirmPassword, confirmPasswordErrorMessage,
    tos, tosErrorMessage) {

    const nonAlphabeticRegex = /[^a-zA-Z]/;
    const specialCharRegex = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/;

    let validFlag = true;
    
    if(!firstName) {
        // console.log('firstName')
        validFlag = false;
        firstNameErrorMessage.textContent = 'Please enter your First Name.';
    } else if(firstName.trim().length < 1 || nonAlphabeticRegex.test(firstName.trim())) {
        // console.log('firstName')
        validFlag = false;
        firstNameErrorMessage.textContent = 'First Name can only include English Alphabets.'
    } else {
        // console.log('firstName')
        firstNameErrorMessage.textContent = '';
    }

    if(!lastName) {
        // console.log('lastName')
        validFlag = false;
        lastNameErrorMessage.textContent = 'Please enter your Last Name.';
    } else if(lastName.trim().length < 1 || nonAlphabeticRegex.test(lastName.trim())) {
        // console.log('lastName')
        validFlag = false;
        lastNameErrorMessage.textContent = 'Last Name can only include English Alphabets.'
    } else {
        // console.log('lastName')
        lastNameErrorMessage.textContent = '';
    }

    if(!email) {
        // console.log('email');
        validFlag = false;
        emailErrorMessage.textContent = 'Please enter your email.';
    } else if(!email.includes('@')) {
        // console.log('email');
        validFlag = false;
        emailErrorMessage.textContent = `Email must contain '@' symbol.`;
    } else {
        // console.log('email');
        emailErrorMessage.textContent = '';
    }

    if(!password) {
        // console.log('password');
        validFlag = false;
        passwordErrorMessage.textContent = 'Please enter a password.';
    } else if(password.trim().length < 8 || !specialCharRegex.test(password.trim())) {
        // console.log('password');
        validFlag = false;
        passwordErrorMessage.textContent = 'Password must be atleast 8 characters in length including 1 special character.';
    } else {
        // console.log('password');
        passwordErrorMessage.textContent = '';
    }
    
    if(!confirmPassword) {
        // console.log('confirmPassword1')
        validFlag = false;
        confirmPasswordErrorMessage.textContent = 'Please cofirm your password.';
    } else if(password !== confirmPassword) {
        // console.log('confirmPassword2')
        validFlag = false;
        confirmPasswordErrorMessage.textContent = `The passwords don't match!`;
    } else {
        // console.log('confirmPassword3')
        confirmPasswordErrorMessage.textContent = '';
    }

    if(!tos.checked) {
        tosErrorMessage.textContent = 'Please agree to the Terms of Service.';
    } else {
        tosErrorMessage.textContent = '';
    }

    return validFlag;
}